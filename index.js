let num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num}  is ${getCube}.`);

const completeAddress = ["258 Washington Ave", "NW, California", "90011"];

const [streetName, city, zipcode] = completeAddress;

console.log(`I live at ${streetName} ${city} ${zipcode}`);

let animal = {
  animalName: "Lolong",
  animalWeight: 1075,
  animalMeasurement: "20 ft 3 in",
};

let { animalName, animalWeight, animalMeasurement } = animal;

console.log(
  `${animalName} was a salwater crocodile. He weighted at ${animalWeight} kgs with a measurement of ${animalMeasurement}`
);

let arrayNumbers = [1, 2, 3, 4, 5];

arrayNumbers.forEach((element) => {
  console.log(element);
});

let reduceNumber = arrayNumbers.reduce((a, b) => a + b);

console.log(reduceNumber);

class dog {
  constructor(dogName, dogAge, dogBreed) {
    this.name = dogName;
    this.age = dogAge;
    this.breed = dogBreed;
  }
}

let dog1 = new dog("Frankie", 5, "Miniature Dachshund");
console.log(dog1);
